docker-polarbytebot-gw2-devcomments-tracker
===========================================

* all actions to the database and all the actual logic is part of the `polarbytebot library <https://gitlab.com/networkjanitor/libpolarbytebot>`_
* this is a small microservice/dockercontainer which queues new comments into the database
* these comments are in this case links to comments made by arenanet employees in the /r/GuildWars2 subreddit